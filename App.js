import React from 'react';
import { Platform, StatusBar, View } from 'react-native';
import { AppLoading } from 'expo';
import AppNavigator from './AppNavigator';
import * as Firebase from 'firebase';
import { YellowBox } from 'react-native';
import _ from 'lodash';

YellowBox.ignoreWarnings(['Setting a timer']);
const _console = _.clone(console);
console.warn = message => {
  if (message.indexOf('Setting a timer') <= -1) {
    _console.warn(message);
  }
};

const config = {
  apiKey: "AIzaSyBAkRsbsA9QD9ecEs7xjyNtBjHDxaIDygk",
  authDomain: "quantic-2951c.firebaseapp.com",
  databaseURL: "https://quantic-2951c.firebaseio.com",
  projectId: "quantic-2951c",
  storageBucket: "",
  messagingSenderId: "270955030278",
  appId: "1:270955030278:web:ee79dd68d83518fd90e087"
};

export default class App extends React.Component {
  state = {
    isLoadingComplete: false,
  };

render() {
  if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
    return (
      <AppLoading
        startAsync={this._loadResourcesAsync}
        onFinish={this._handleFinishLoading}
      />
    );
  } else {
    return (
      <View style={{flex:1}}>
        {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
        <AppNavigator />
      </View>
    );
  }
}

_loadResourcesAsync = async () => {
  Firebase.initializeApp(config);
  require("firebase/firestore");
};

_handleFinishLoading = () => {
  this.setState({ isLoadingComplete: true });
};
}
