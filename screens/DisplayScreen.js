import React, { Component } from 'react';
import { StyleSheet, Text, View, FlatList, Dimensions, Button, Alert } from 'react-native';
import * as Firebase from 'firebase';

export default class DataQuery extends React.Component {
  static navigationOptions = {
    title: 'Daten anzeigen',
    headerStyle: {
      backgroundColor: '#2C2E3C',
    },
    headerTitleStyle: {
      color: 'white',
    },
  }
  constructor(props) {
    super(props);
    this.state = ({
      ausgabe: [],
    })
    gibDaten = [];
    var user = Firebase.auth().currentUser;
    var db = Firebase.firestore();
    db.collection(user.email)
      .get()
      .then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          gibDaten.push({
            first: doc.data().first,
            last: doc.data().last,
            born: doc.data().born
          })
      });
      this.setState({
        ausgabe: gibDaten,
      })
    });
  }

  onAddPress = () => {
    this.props.navigation.navigate('Main');
  }

  render() {
    return (
      <View style={{padding: 20, backgroundColor: '#2C2E3C', height: 2000}}>
        <View style={{paddingBottom: 20}}>
        <Button 
                onPress={this.onAddPress}
                title="Person hinzufügen"
                ref={'add'}
        />
        </View>
        
        <FlatList 
          data={this.state.ausgabe}
          renderItem={({ item }) => (
            <View><Text style={{color: 'white'}}> {item.first + ' ' + item.last + ' ' + item.born} </Text></View>
          )}
          keyExtractor={(item, index) => index.toString()}
          />
            
      </View>
    );
  };
}
  
