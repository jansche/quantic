import React, { Component } from 'react';
import {
    ScrollView,
    TextInput,
    View,
    Button,
    KeyboardAvoidingView,
} from 'react-native';
import * as Firebase from 'firebase';

export default class Register extends Component {
  static navigationOptions = {
    title: 'Register',
    headerStyle: {
      backgroundColor: '#2C2E3C',
    },
    headerTitleStyle: {
      color: 'white',
    },
  }
  state = {
    EMailAddress: '',
    Password: '',
  }

  onRegisterPress = () => {
    Firebase.auth().createUserWithEmailAndPassword(this.state.EMailAddress, this.state.Password)
    .catch(error => {
        switch (error.code) {
          case 'auth/email-already-in-use':
            alert('Diese E-Mail Adresse ist bereits bei uns hinterlegt.');
            break;
          case 'auth/invalid-email':
            alert('Diese E-Mail Adresse ist ungültig.');
            break;
          case 'auth/operation-not-allowed':
            alert('Unbekannter Fehler.');
            break;
          case 'auth/weak-password':
            alert('Passwort ist zu schwach.');
            break;
          default:
            alert('Unbekannter Fehler')
        }
    })
    .then(u => {
      if (Firebase.auth().currentUser) {
        this.props.navigation.navigate('Display');
      }
      else {
        Firebase.auth().signInWithEmailAndPassword(this.state.EMailAddress, this.state.Password);
        this.props.navigation.navigate('Display');
      }
    })
  }


  onSubmitEditingEMail = () => {
    this.refs.Password.focus();
  }

  render() {
    return (
      <ScrollView style={{ padding: 20, backgroundColor: '#2C2E3C',}}>
        <TextInput 
          placeholder='E-Mail Adresse' 
          keyboardType={'email-address'} 
          style={{ margin: 20, color: 'white'}}
          ref={'EMailAddress'} 
          keyboardType={'email-address'} 
          onSubmitEditing={this.onSubmitEditingEMail} 
          onChangeText={(EMailAddress) => this.setState({EMailAddress})}
        />
        <TextInput placeholder='Passwort' 
          style={{ margin: 20, color: 'white'}}
          ref={'Password'} 
          secureTextEntry={true} 
          onSubmitEditing={this.onRegisterPress} 
          onChangeText={(Password) => this.setState({Password})}
        />
        <View style={{ margin: 10 }} />
        <KeyboardAvoidingView behavior="padding">
          <Button 
            onPress={this.onRegisterPress}
            title='Register'
            ref={'Register'} />
        </KeyboardAvoidingView>
      </ScrollView> 
      );
  }
}
