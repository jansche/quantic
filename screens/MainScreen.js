import React, { Component } from 'react';
import {Alert, Button, StyleSheet, Text, TextInput, View } from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import * as Firebase from 'firebase';

export default class App extends Component {
  static navigationOptions = {
    title: 'Datum speichern',
    headerStyle: {
      backgroundColor: '#2C2E3C',
    },
    headerTitleStyle: {
      color: 'white',
    },
  }
  constructor() {
    super()
    this.state = {
      isVisible: false,
      chosenDate: '',
      givenname: '',
      surname: '',
    }
  }

  handlePicker = (date) => {
    this.setState({
      isVisible: false,
      chosenDate: moment(date).format('DD. MMMM YYYY')
    })
  }

  showPicker = () =>{
    this.setState({
      isVisible: true
    })
  }

  hidePicker = () => {
    this.setState({
      isVisible: false
    })
  }

  onSubmitGivenname = () => {
    this.refs.surname.focus();
  }

  onSubmitEditing = () => {
    this.showPicker();
  }

  onFinishedPress = () => {
    var user = Firebase.auth().currentUser;
    var db = Firebase.firestore();
    db.collection(user.email).add({
        first: this.state.givenname,
        last: this.state.surname,
        born: this.state.chosenDate,
    })
    .then(function(docRef) {
        console.log("Document written with ID: ", docRef.id);
    })
    .catch(function(error) {
        console.error("Error adding document: ", error);
    });
    Alert.alert('Geburtsdatum gespeichert');
    this.props.navigation.navigate('Display');
  }

  render(){
    return (
      <View style={styles.container}>
        <TextInput
          placeholder='Vorname'
          style={{ margin: 20, color: 'white'}} 
          ref={'givenname'} 
          returnKeyType='next' 
          onSubmitEditing={this.onSubmitGivenname} 
          onChangeText={(givenname) => this.setState({givenname})}/>

        <TextInput 
          placeholder='Nachname'
          style={{ margin: 20, color: 'white'}} 
          ref={'surname'} 
          returnKeyType='next' 
          onSubmitEditing={this.onSubmitEditing} 
          onChangeText={(surname) => this.setState({surname})}/>

        <Button 
          onPress={this.showPicker}
          title='Datum auswählen'/>

        <DateTimePicker
          isVisible={this.state.isVisible}
          onConfirm={this.handlePicker}
          onCancel={this.hidePicker}
          mode={'date'}
          datePickerModeAndroid={'spinner'}
          is24Hour={true} />

        <Text style={{color: 'white', margin: 20, fontSize: 13}}>
          {this.state.chosenDate}
        </Text>

        <View style={{ marginTop: 300 }}>
          <Button
            title='Speichern'
            onPress={this.onFinishedPress} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: '#2C2E3C',
  },
});
