import React, { Component } from 'react';

import {
    ScrollView,
    TextInput,
    View,
    Button,
    KeyboardAvoidingView,
} from 'react-native';
import * as Firebase from 'firebase';

export default class Login extends Component {
  static navigationOptions = {
    title: 'Login',
    headerStyle: {
      backgroundColor: '#2C2E3C',
    },
    headerTitleStyle: {
      color: 'white',
    },
  }
  state = {
    EMailAddress: '',
    Password: '',
  }

  onLoginPress = () => {
    Firebase.auth().signInWithEmailAndPassword(this.state.EMailAddress, this.state.Password)
    .then(u => {this.props.navigation.navigate('Display')})
    .catch(function(error) {
      alert('Falsche Eingabe');
    });
  }

  onRegisterPress = () => {
    this.props.navigation.navigate('Register');
  }

  onSubmitEditing = () => {
    this.refs.Password.focus();
  }

  render() {
    return (
      <ScrollView style={{ padding: 20, backgroundColor: '#2C2E3C', }}>
        <TextInput placeholder='E-Mail Adresse' 
          keyboardType={'email-address'} 
          style={{ margin: 20, color: 'white'}}
          ref={'EMailAddress'} 
          returnKeyType="next"
          onSubmitEditing={this.onSubmitEditing} 
          onChangeText={(EMailAddress) => this.setState({EMailAddress})}
        />
        <TextInput placeholder='Passwort' 
        style={{ margin: 20, color: 'white'}}
        ref={'Password'} 
        secureTextEntry={true} 
        onSubmitEditing={this.onLoginPress} 
        onChangeText={(Password) => this.setState({Password})}/>
        <View style={{ margin: 10 }} />
        <KeyboardAvoidingView behavior="padding">
          <View>
            <Button 
              onPress={this.onLoginPress}
              title="Log me in"
              ref={'Anmelden'}
            />
           </View>
           <View style={{ marginTop: 330 }}>
            <Button 
                onPress={this.onRegisterPress}
                title='Sign me up'
                ref={'Regristrieren'}
              />
           </View>
           
        </KeyboardAvoidingView>
      </ScrollView> 
      );
  }
}
