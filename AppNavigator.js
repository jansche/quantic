import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import LoginScreen from './screens/LoginScreen';
import RegisterScreen from './screens/RegisterScreen';
import MainScreen from './screens/MainScreen';
import DisplayScreen from './screens/DisplayScreen';

const LoginStack = createStackNavigator({ Login: LoginScreen });
const RegisterStack = createStackNavigator({ Register: RegisterScreen});
const MainStack = createStackNavigator({ Main: MainScreen});
const DisplayStack = createStackNavigator({ Display: DisplayScreen});

const SwitchNavigator = createSwitchNavigator(
  {
    Login: LoginStack,
    Register: RegisterStack,
    Main: MainStack,
    Display: DisplayStack,
  },
  {
    initialRouteName: 'Login',
  }
);

const AppContainer = createAppContainer(SwitchNavigator);

export default AppContainer;